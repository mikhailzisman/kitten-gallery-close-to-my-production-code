Kittenapp
=========
This code for my code style purposes only.

Simple and funny app for placekitten.com service

The whole project is divided logically into modules UI, Data, API according to the SRP and IoC principle.

You can build this app with

Windows : gradlew build

Linux/MacOS : sh sudo gradlew build

Thank you!

Developer: Mikhail Zisman (zisman.mikhail@gmail.com)